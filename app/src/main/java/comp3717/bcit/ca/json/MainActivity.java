package comp3717.bcit.ca.json;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity
    extends AppCompatActivity
{
    private final String TAG = MainActivity.class.getName();

    private EditText jsonText;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        jsonText = (EditText)findViewById(R.id.editText);
    }

    public void showJSON(final View view)
    {
        final String jsonString;

        jsonString = jsonText.getText().toString();

        try
        {
            final JSONObject json;
            final String     value;

            json = new JSONObject(jsonString);
            value = json.getString("x");
            Toast.makeText(this, "x = " + value, Toast.LENGTH_LONG).show();
        }
        catch(final JSONException ex)
        {
            Toast.makeText(this, "Invalid JSON", Toast.LENGTH_LONG).show();
        }
    }
}
